//
//  main.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

// MARK: - Settings

let numberOfEngineers = 62
let newEspressoAfterMinutes = 60 // One cofee an hour
let espressosPerMinuteByMachine = 1

let chanceToBecomeBusy: Float = 0.2
let decideBusinessEveryMinutes = 10

let endOfTime = 8*60


// MARK: - Program

var engineers = [Engineer]()
var machine = Espresso()


// Create engineers
for i in 1...numberOfEngineers {
    let engineer = Engineer(id: i, isBusy: randomBusy(), gotCoffeeAt: nil)
    engineers.append(engineer)
    machine.enqueue(engineer)
}

// State before machine is turned on
print("0 time:")
print(queue: machine.queueHighPriority, name: "Busy queue    ")
print(queue: machine.queueLowPriority, name: "Non-Busy queue")
print("---")


for time in 1...endOfTime {
    
    print("Step \(time):")
    
    // Change busyness of programmers
    if time % decideBusinessEveryMinutes == 0 {
        print("Busyness changed")
        for engineer in engineers {
            engineer.isBusy = randomBusy()
        }
        machine.moveEngineersToProperQueues()
    }
    
    // Give coffee to an engineer!
    for _ in 1...espressosPerMinuteByMachine {
        guard let engineer = machine.dequeue() else {
            continue
        }
        engineer.gotCoffeeAt = time
    }
    
    // Bring back engineers who drunk coffee long time ago
    for engineer in engineers {
        guard let gotCoffeeAt = engineer.gotCoffeeAt else { continue }
        let noCoffeeTime = time - gotCoffeeAt
        if noCoffeeTime >= newEspressoAfterMinutes {
            engineer.gotCoffeeAt = nil
            machine.enqueue(engineer)
        }
    }
    
    
    print(queue: machine.queueHighPriority, name: "Busy queue    ")
    print(queue: machine.queueLowPriority, name: "Non-Busy queue")
    print("---")
    
}

