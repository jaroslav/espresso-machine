//
//  Espresso.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

/// Espresso machine queue simulator
struct Espresso{
    
    var queueLowPriority = Queue()
    var queueHighPriority = Queue()
    
    /// Add engineer to proper queue
    mutating func enqueue(_ engineer: Engineer){
        if engineer.isBusy {
            queueHighPriority.enqueue(engineer)
        }else{
            queueLowPriority.enqueue(engineer)
        }
    }
    
    /// Return engineer qhos eligible to receive next cup of coffee
    mutating func dequeue() -> Engineer? {
        if let engineer = queueHighPriority.dequeue() {
            return engineer
        }
        if let engineer = queueLowPriority.dequeue() {
            return engineer
        }
        return nil
    }
    
    mutating func moveEngineersToProperQueues(){
        for engineer in queueHighPriority.list {
            if !engineer.isBusy{
                queueHighPriority.remove(engineer)
                queueLowPriority.enqueue(engineer)
            }
        }
        for engineer in queueLowPriority.list {
            if engineer.isBusy{
                queueLowPriority.remove(engineer)
                queueHighPriority.enqueue(engineer)
            }
        }
    }
    
}

