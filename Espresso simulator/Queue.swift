//
//  Queue.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation


/// Queue data structure - FIFO
struct Queue {
    var list = [Engineer]()
    
    var isEmpty: Bool {
        return list.isEmpty
    }
    
    mutating func enqueue(_ element: Engineer) {
        list.append(element)
    }
    
    mutating func dequeue() -> Engineer? {
        guard !list.isEmpty else {
            return nil
        }
        return list.removeFirst()
    }
    
    mutating func remove(_ object: Engineer) {
        list = list.filter{ $0 != object }
    }
    
}
