//
//  Array.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    /// Remove first collection element that is equal to the given `object`
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
