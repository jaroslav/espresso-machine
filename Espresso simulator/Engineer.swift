//
//  Engineer.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

class Engineer: Equatable{
    
    public let id: Int
    public var isBusy: Bool
    public var gotCoffeeAt: Int?
    
    init(id: Int, isBusy: Bool, gotCoffeeAt: Int? = nil){
        self.id = id
        self.isBusy = isBusy
        self.gotCoffeeAt = gotCoffeeAt
    }
    
    static func == (lhs: Engineer, rhs: Engineer) -> Bool {
        return lhs.id == rhs.id
    }
    
}

