//
//  functions.swift
//  Espresso simulator
//
//  Created by Jaroslav on 25-10-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

/// Print queue to console
func print(queue: Queue, name: String){
    var str = ""
    for engineer in queue.list {
        str.append(String(format: "%i;", engineer.id))
    }
    let result = String(format: "%@: %@", name, str.count > 0 ? str : "empty")
    print(result)
}

// Random busy/non-busy generator
func randomBusy() -> Bool{
    return Float.random(in: 0...1) < chanceToBecomeBusy
}

