#  Espresso machine simulator


## Running program

Just change parameters and run program in XCode 10.


## Settings

`numberOfEngineers` - number of engineers in the office

`newEspressoAfterMinutes` - after this time passes engineer wantd another cup of coffee

`espressosPerMinuteByMachine` - how many espresso cups can be made by machine


`chanceToBecomeBusy` - chance that engineer will become busy

`decideBusinessEveryMinutes` - how long engineers stay busy/non-busy


`endOfTime` - length of working day

## Example 


### Settings

```
let numberOfEngineers = 62
let newEspressoAfterMinutes = 60 // One cofee an hour
let espressosPerMinuteByMachine = 1

let chanceToBecomeBusy: Float = 0.2
let decideBusinessEveryMinutes = 10

let endOfTime = 8*60
```

### Output

```
0 time:
Busy queue    : 2;9;15;24;27;29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 1:
Busy queue    : 9;15;24;27;29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 2:
Busy queue    : 15;24;27;29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 3:
Busy queue    : 24;27;29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 4:
Busy queue    : 27;29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 5:
Busy queue    : 29;46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 6:
Busy queue    : 46;56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 7:
Busy queue    : 56;58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 8:
Busy queue    : 58;61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 9:
Busy queue    : 61;
Non-Busy queue: 1;3;4;5;6;7;8;10;11;12;13;14;16;17;18;19;20;21;22;23;25;26;28;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;47;48;49;50;51;52;53;54;55;57;59;60;62;
---
Step 10:
Busyness changed
Busy queue    : 6;8;12;14;19;30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 11:
Busy queue    : 8;12;14;19;30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 12:
Busy queue    : 12;14;19;30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 13:
Busy queue    : 14;19;30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 14:
Busy queue    : 19;30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 15:
Busy queue    : 30;35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 16:
Busy queue    : 35;41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 17:
Busy queue    : 41;55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 18:
Busy queue    : 55;60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 19:
Busy queue    : 60;
Non-Busy queue: 1;3;4;7;10;11;13;16;17;18;20;21;22;23;25;26;28;31;32;33;34;36;37;38;39;40;42;43;44;45;47;48;49;50;51;52;53;54;57;59;62;61;
---
Step 20:
Busyness changed
Busy queue    : 13;18;22;31;38;44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 21:
Busy queue    : 18;22;31;38;44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 22:
Busy queue    : 22;31;38;44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 23:
Busy queue    : 31;38;44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 24:
Busy queue    : 38;44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 25:
Busy queue    : 44;45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 26:
Busy queue    : 45;59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 27:
Busy queue    : 59;
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 28:
Busy queue    : empty
Non-Busy queue: 3;4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 29:
Busy queue    : empty
Non-Busy queue: 4;7;10;11;16;17;20;21;23;25;26;28;32;33;34;36;37;39;40;42;43;47;48;49;50;51;52;53;54;57;62;61;60;
---
Step 30:
Busyness changed
Busy queue    : 21;25;33;34;40;48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 31:
Busy queue    : 25;33;34;40;48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 32:
Busy queue    : 33;34;40;48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 33:
Busy queue    : 34;40;48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 34:
Busy queue    : 40;48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 35:
Busy queue    : 48;57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 36:
Busy queue    : 57;
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 37:
Busy queue    : empty
Non-Busy queue: 4;7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 38:
Busy queue    : empty
Non-Busy queue: 7;10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 39:
Busy queue    : empty
Non-Busy queue: 10;11;16;17;23;26;28;32;36;37;39;42;43;47;49;50;51;52;53;54;62;61;60;
---
Step 40:
Busyness changed
Busy queue    : 47;51;62;
Non-Busy queue: 10;11;16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 41:
Busy queue    : 51;62;
Non-Busy queue: 10;11;16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 42:
Busy queue    : 62;
Non-Busy queue: 10;11;16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 43:
Busy queue    : empty
Non-Busy queue: 10;11;16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 44:
Busy queue    : empty
Non-Busy queue: 11;16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 45:
Busy queue    : empty
Non-Busy queue: 16;17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 46:
Busy queue    : empty
Non-Busy queue: 17;23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 47:
Busy queue    : empty
Non-Busy queue: 23;28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 48:
Busy queue    : empty
Non-Busy queue: 28;32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 49:
Busy queue    : empty
Non-Busy queue: 32;36;37;39;42;43;49;50;52;53;54;61;60;
---
Step 50:
Busyness changed
Busy queue    : 54;61;60;
Non-Busy queue: 32;36;37;39;42;43;49;50;52;
---
Step 51:
Busy queue    : 61;60;
Non-Busy queue: 32;36;37;39;42;43;49;50;52;
---
Step 52:
Busy queue    : 60;
Non-Busy queue: 32;36;37;39;42;43;49;50;52;
---
Step 53:
Busy queue    : empty
Non-Busy queue: 32;36;37;39;42;43;49;50;52;
---
Step 54:
Busy queue    : empty
Non-Busy queue: 36;37;39;42;43;49;50;52;
---
Step 55:
Busy queue    : empty
Non-Busy queue: 37;39;42;43;49;50;52;
---
Step 56:
Busy queue    : empty
Non-Busy queue: 39;42;43;49;50;52;
---
Step 57:
Busy queue    : empty
Non-Busy queue: 42;43;49;50;52;
---
Step 58:
Busy queue    : empty
Non-Busy queue: 43;49;50;52;
---
Step 59:
Busy queue    : empty
Non-Busy queue: 49;50;52;
---
Step 60:
Busyness changed
Busy queue    : empty
Non-Busy queue: 50;52;
---
Step 61:
Busy queue    : empty
Non-Busy queue: 52;2;
---
Step 62:
Busy queue    : empty
Non-Busy queue: 2;9;
---
Step 63:
Busy queue    : empty
Non-Busy queue: 9;15;
---
Step 64:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 65:
Busy queue    : empty
Non-Busy queue: 24;27;
---
Step 66:
Busy queue    : empty
Non-Busy queue: 27;29;
---
Step 67:
Busy queue    : empty
Non-Busy queue: 29;46;
---
Step 68:
Busy queue    : 56;
Non-Busy queue: 46;
---
Step 69:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 70:
Busyness changed
Busy queue    : 58;
Non-Busy queue: 5;
---
Step 71:
Busy queue    : 6;
Non-Busy queue: 5;
---
Step 72:
Busy queue    : 8;
Non-Busy queue: 5;
---
Step 73:
Busy queue    : empty
Non-Busy queue: 5;12;
---
Step 74:
Busy queue    : 14;
Non-Busy queue: 12;
---
Step 75:
Busy queue    : empty
Non-Busy queue: 12;19;
---
Step 76:
Busy queue    : empty
Non-Busy queue: 19;30;
---
Step 77:
Busy queue    : empty
Non-Busy queue: 30;35;
---
Step 78:
Busy queue    : 41;
Non-Busy queue: 35;
---
Step 79:
Busy queue    : empty
Non-Busy queue: 35;55;
---
Step 80:
Busyness changed
Busy queue    : empty
Non-Busy queue: 55;1;
---
Step 81:
Busy queue    : empty
Non-Busy queue: 1;13;
---
Step 82:
Busy queue    : empty
Non-Busy queue: 13;18;
---
Step 83:
Busy queue    : empty
Non-Busy queue: 18;22;
---
Step 84:
Busy queue    : empty
Non-Busy queue: 22;31;
---
Step 85:
Busy queue    : empty
Non-Busy queue: 31;38;
---
Step 86:
Busy queue    : empty
Non-Busy queue: 38;44;
---
Step 87:
Busy queue    : empty
Non-Busy queue: 44;45;
---
Step 88:
Busy queue    : empty
Non-Busy queue: 45;59;
---
Step 89:
Busy queue    : empty
Non-Busy queue: 59;3;
---
Step 90:
Busyness changed
Busy queue    : empty
Non-Busy queue: 3;20;
---
Step 91:
Busy queue    : 21;
Non-Busy queue: 20;
---
Step 92:
Busy queue    : 25;
Non-Busy queue: 20;
---
Step 93:
Busy queue    : 33;
Non-Busy queue: 20;
---
Step 94:
Busy queue    : empty
Non-Busy queue: 20;34;
---
Step 95:
Busy queue    : empty
Non-Busy queue: 34;40;
---
Step 96:
Busy queue    : empty
Non-Busy queue: 40;48;
---
Step 97:
Busy queue    : empty
Non-Busy queue: 48;57;
---
Step 98:
Busy queue    : 4;
Non-Busy queue: 57;
---
Step 99:
Busy queue    : 7;
Non-Busy queue: 57;
---
Step 100:
Busyness changed
Busy queue    : empty
Non-Busy queue: 7;26;
---
Step 101:
Busy queue    : 47;
Non-Busy queue: 26;
---
Step 102:
Busy queue    : 51;
Non-Busy queue: 26;
---
Step 103:
Busy queue    : empty
Non-Busy queue: 26;62;
---
Step 104:
Busy queue    : 10;
Non-Busy queue: 62;
---
Step 105:
Busy queue    : empty
Non-Busy queue: 62;11;
---
Step 106:
Busy queue    : 16;
Non-Busy queue: 11;
---
Step 107:
Busy queue    : empty
Non-Busy queue: 11;17;
---
Step 108:
Busy queue    : empty
Non-Busy queue: 17;23;
---
Step 109:
Busy queue    : 28;
Non-Busy queue: 23;
---
Step 110:
Busyness changed
Busy queue    : 23;
Non-Busy queue: 53;
---
Step 111:
Busy queue    : empty
Non-Busy queue: 53;54;
---
Step 112:
Busy queue    : empty
Non-Busy queue: 54;61;
---
Step 113:
Busy queue    : empty
Non-Busy queue: 61;60;
---
Step 114:
Busy queue    : empty
Non-Busy queue: 60;32;
---
Step 115:
Busy queue    : empty
Non-Busy queue: 32;36;
---
Step 116:
Busy queue    : empty
Non-Busy queue: 36;37;
---
Step 117:
Busy queue    : empty
Non-Busy queue: 37;39;
---
Step 118:
Busy queue    : empty
Non-Busy queue: 39;42;
---
Step 119:
Busy queue    : empty
Non-Busy queue: 42;43;
---
Step 120:
Busyness changed
Busy queue    : empty
Non-Busy queue: 43;49;
---
Step 121:
Busy queue    : empty
Non-Busy queue: 49;50;
---
Step 122:
Busy queue    : 52;
Non-Busy queue: 50;
---
Step 123:
Busy queue    : empty
Non-Busy queue: 50;2;
---
Step 124:
Busy queue    : empty
Non-Busy queue: 2;9;
---
Step 125:
Busy queue    : empty
Non-Busy queue: 9;15;
---
Step 126:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 127:
Busy queue    : empty
Non-Busy queue: 24;27;
---
Step 128:
Busy queue    : 29;
Non-Busy queue: 27;
---
Step 129:
Busy queue    : empty
Non-Busy queue: 27;56;
---
Step 130:
Busyness changed
Busy queue    : empty
Non-Busy queue: 27;46;
---
Step 131:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 132:
Busy queue    : empty
Non-Busy queue: 58;6;
---
Step 133:
Busy queue    : empty
Non-Busy queue: 6;8;
---
Step 134:
Busy queue    : empty
Non-Busy queue: 8;5;
---
Step 135:
Busy queue    : 14;
Non-Busy queue: 5;
---
Step 136:
Busy queue    : empty
Non-Busy queue: 5;12;
---
Step 137:
Busy queue    : empty
Non-Busy queue: 12;19;
---
Step 138:
Busy queue    : empty
Non-Busy queue: 19;30;
---
Step 139:
Busy queue    : empty
Non-Busy queue: 30;41;
---
Step 140:
Busyness changed
Busy queue    : empty
Non-Busy queue: 41;35;
---
Step 141:
Busy queue    : empty
Non-Busy queue: 35;55;
---
Step 142:
Busy queue    : 1;
Non-Busy queue: 55;
---
Step 143:
Busy queue    : empty
Non-Busy queue: 55;13;
---
Step 144:
Busy queue    : empty
Non-Busy queue: 13;18;
---
Step 145:
Busy queue    : empty
Non-Busy queue: 18;22;
---
Step 146:
Busy queue    : empty
Non-Busy queue: 22;31;
---
Step 147:
Busy queue    : empty
Non-Busy queue: 31;38;
---
Step 148:
Busy queue    : 44;
Non-Busy queue: 38;
---
Step 149:
Busy queue    : empty
Non-Busy queue: 38;45;
---
Step 150:
Busyness changed
Busy queue    : empty
Non-Busy queue: 45;59;
---
Step 151:
Busy queue    : empty
Non-Busy queue: 59;3;
---
Step 152:
Busy queue    : empty
Non-Busy queue: 3;21;
---
Step 153:
Busy queue    : 25;
Non-Busy queue: 21;
---
Step 154:
Busy queue    : empty
Non-Busy queue: 21;33;
---
Step 155:
Busy queue    : empty
Non-Busy queue: 33;20;
---
Step 156:
Busy queue    : empty
Non-Busy queue: 20;34;
---
Step 157:
Busy queue    : empty
Non-Busy queue: 34;40;
---
Step 158:
Busy queue    : empty
Non-Busy queue: 40;48;
---
Step 159:
Busy queue    : empty
Non-Busy queue: 48;4;
---
Step 160:
Busyness changed
Busy queue    : empty
Non-Busy queue: 4;57;
---
Step 161:
Busy queue    : empty
Non-Busy queue: 57;7;
---
Step 162:
Busy queue    : 47;
Non-Busy queue: 7;
---
Step 163:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 164:
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 165:
Busy queue    : empty
Non-Busy queue: 26;10;
---
Step 166:
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 167:
Busy queue    : empty
Non-Busy queue: 62;16;
---
Step 168:
Busy queue    : empty
Non-Busy queue: 16;11;
---
Step 169:
Busy queue    : empty
Non-Busy queue: 11;17;
---
Step 170:
Busyness changed
Busy queue    : empty
Non-Busy queue: 17;28;
---
Step 171:
Busy queue    : empty
Non-Busy queue: 28;23;
---
Step 172:
Busy queue    : empty
Non-Busy queue: 23;53;
---
Step 173:
Busy queue    : empty
Non-Busy queue: 53;54;
---
Step 174:
Busy queue    : empty
Non-Busy queue: 54;61;
---
Step 175:
Busy queue    : empty
Non-Busy queue: 61;60;
---
Step 176:
Busy queue    : 32;
Non-Busy queue: 60;
---
Step 177:
Busy queue    : 36;
Non-Busy queue: 60;
---
Step 178:
Busy queue    : empty
Non-Busy queue: 60;37;
---
Step 179:
Busy queue    : 39;
Non-Busy queue: 37;
---
Step 180:
Busyness changed
Busy queue    : empty
Non-Busy queue: 39;42;
---
Step 181:
Busy queue    : empty
Non-Busy queue: 42;43;
---
Step 182:
Busy queue    : empty
Non-Busy queue: 43;49;
---
Step 183:
Busy queue    : empty
Non-Busy queue: 49;52;
---
Step 184:
Busy queue    : empty
Non-Busy queue: 52;50;
---
Step 185:
Busy queue    : 2;
Non-Busy queue: 50;
---
Step 186:
Busy queue    : 9;
Non-Busy queue: 50;
---
Step 187:
Busy queue    : empty
Non-Busy queue: 50;15;
---
Step 188:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 189:
Busy queue    : empty
Non-Busy queue: 24;29;
---
Step 190:
Busyness changed
Busy queue    : empty
Non-Busy queue: 24;56;
---
Step 191:
Busy queue    : 27;
Non-Busy queue: 56;
---
Step 192:
Busy queue    : empty
Non-Busy queue: 56;46;
---
Step 193:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 194:
Busy queue    : empty
Non-Busy queue: 58;6;
---
Step 195:
Busy queue    : empty
Non-Busy queue: 6;8;
---
Step 196:
Busy queue    : empty
Non-Busy queue: 8;14;
---
Step 197:
Busy queue    : empty
Non-Busy queue: 14;5;
---
Step 198:
Busy queue    : empty
Non-Busy queue: 5;12;
---
Step 199:
Busy queue    : empty
Non-Busy queue: 12;19;
---
Step 200:
Busyness changed
Busy queue    : empty
Non-Busy queue: 19;30;
---
Step 201:
Busy queue    : 41;
Non-Busy queue: 30;
---
Step 202:
Busy queue    : empty
Non-Busy queue: 30;35;
---
Step 203:
Busy queue    : empty
Non-Busy queue: 35;1;
---
Step 204:
Busy queue    : 55;
Non-Busy queue: 1;
---
Step 205:
Busy queue    : empty
Non-Busy queue: 1;13;
---
Step 206:
Busy queue    : empty
Non-Busy queue: 13;18;
---
Step 207:
Busy queue    : empty
Non-Busy queue: 18;22;
---
Step 208:
Busy queue    : empty
Non-Busy queue: 22;31;
---
Step 209:
Busy queue    : empty
Non-Busy queue: 31;44;
---
Step 210:
Busyness changed
Busy queue    : 38;
Non-Busy queue: 44;
---
Step 211:
Busy queue    : 45;
Non-Busy queue: 44;
---
Step 212:
Busy queue    : 59;
Non-Busy queue: 44;
---
Step 213:
Busy queue    : empty
Non-Busy queue: 44;3;
---
Step 214:
Busy queue    : 25;
Non-Busy queue: 3;
---
Step 215:
Busy queue    : empty
Non-Busy queue: 3;21;
---
Step 216:
Busy queue    : empty
Non-Busy queue: 21;33;
---
Step 217:
Busy queue    : 20;
Non-Busy queue: 33;
---
Step 218:
Busy queue    : empty
Non-Busy queue: 33;34;
---
Step 219:
Busy queue    : empty
Non-Busy queue: 34;40;
---
Step 220:
Busyness changed
Busy queue    : 40;
Non-Busy queue: 48;
---
Step 221:
Busy queue    : empty
Non-Busy queue: 48;4;
---
Step 222:
Busy queue    : 57;
Non-Busy queue: 4;
---
Step 223:
Busy queue    : empty
Non-Busy queue: 4;47;
---
Step 224:
Busy queue    : empty
Non-Busy queue: 47;7;
---
Step 225:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 226:
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 227:
Busy queue    : empty
Non-Busy queue: 26;10;
---
Step 228:
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 229:
Busy queue    : empty
Non-Busy queue: 62;16;
---
Step 230:
Busyness changed
Busy queue    : 16;
Non-Busy queue: 11;
---
Step 231:
Busy queue    : empty
Non-Busy queue: 11;17;
---
Step 232:
Busy queue    : empty
Non-Busy queue: 17;28;
---
Step 233:
Busy queue    : empty
Non-Busy queue: 28;23;
---
Step 234:
Busy queue    : empty
Non-Busy queue: 23;53;
---
Step 235:
Busy queue    : empty
Non-Busy queue: 53;54;
---
Step 236:
Busy queue    : empty
Non-Busy queue: 54;61;
---
Step 237:
Busy queue    : 32;
Non-Busy queue: 61;
---
Step 238:
Busy queue    : empty
Non-Busy queue: 61;36;
---
Step 239:
Busy queue    : empty
Non-Busy queue: 36;60;
---
Step 240:
Busyness changed
Busy queue    : empty
Non-Busy queue: 60;37;
---
Step 241:
Busy queue    : empty
Non-Busy queue: 37;39;
---
Step 242:
Busy queue    : empty
Non-Busy queue: 39;42;
---
Step 243:
Busy queue    : 43;
Non-Busy queue: 42;
---
Step 244:
Busy queue    : empty
Non-Busy queue: 42;49;
---
Step 245:
Busy queue    : empty
Non-Busy queue: 49;52;
---
Step 246:
Busy queue    : empty
Non-Busy queue: 52;2;
---
Step 247:
Busy queue    : empty
Non-Busy queue: 2;9;
---
Step 248:
Busy queue    : empty
Non-Busy queue: 9;50;
---
Step 249:
Busy queue    : 15;
Non-Busy queue: 50;
---
Step 250:
Busyness changed
Busy queue    : 29;
Non-Busy queue: 15;
---
Step 251:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 252:
Busy queue    : empty
Non-Busy queue: 24;27;
---
Step 253:
Busy queue    : empty
Non-Busy queue: 27;56;
---
Step 254:
Busy queue    : empty
Non-Busy queue: 56;46;
---
Step 255:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 256:
Busy queue    : empty
Non-Busy queue: 58;6;
---
Step 257:
Busy queue    : empty
Non-Busy queue: 6;8;
---
Step 258:
Busy queue    : empty
Non-Busy queue: 8;14;
---
Step 259:
Busy queue    : empty
Non-Busy queue: 14;5;
---
Step 260:
Busyness changed
Busy queue    : 12;
Non-Busy queue: 5;
---
Step 261:
Busy queue    : empty
Non-Busy queue: 5;19;
---
Step 262:
Busy queue    : 41;
Non-Busy queue: 19;
---
Step 263:
Busy queue    : 30;
Non-Busy queue: 19;
---
Step 264:
Busy queue    : empty
Non-Busy queue: 19;35;
---
Step 265:
Busy queue    : empty
Non-Busy queue: 35;55;
---
Step 266:
Busy queue    : empty
Non-Busy queue: 55;1;
---
Step 267:
Busy queue    : 13;
Non-Busy queue: 1;
---
Step 268:
Busy queue    : empty
Non-Busy queue: 1;18;
---
Step 269:
Busy queue    : empty
Non-Busy queue: 18;22;
---
Step 270:
Busyness changed
Busy queue    : empty
Non-Busy queue: 22;31;
---
Step 271:
Busy queue    : 38;
Non-Busy queue: 31;
---
Step 272:
Busy queue    : 45;
Non-Busy queue: 31;
---
Step 273:
Busy queue    : empty
Non-Busy queue: 31;59;
---
Step 274:
Busy queue    : empty
Non-Busy queue: 59;44;
---
Step 275:
Busy queue    : empty
Non-Busy queue: 44;25;
---
Step 276:
Busy queue    : empty
Non-Busy queue: 25;3;
---
Step 277:
Busy queue    : empty
Non-Busy queue: 3;21;
---
Step 278:
Busy queue    : empty
Non-Busy queue: 21;20;
---
Step 279:
Busy queue    : empty
Non-Busy queue: 20;33;
---
Step 280:
Busyness changed
Busy queue    : empty
Non-Busy queue: 33;34;
---
Step 281:
Busy queue    : empty
Non-Busy queue: 34;40;
---
Step 282:
Busy queue    : empty
Non-Busy queue: 40;48;
---
Step 283:
Busy queue    : empty
Non-Busy queue: 48;57;
---
Step 284:
Busy queue    : empty
Non-Busy queue: 57;4;
---
Step 285:
Busy queue    : empty
Non-Busy queue: 4;47;
---
Step 286:
Busy queue    : empty
Non-Busy queue: 47;7;
---
Step 287:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 288:
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 289:
Busy queue    : 10;
Non-Busy queue: 26;
---
Step 290:
Busyness changed
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 291:
Busy queue    : empty
Non-Busy queue: 62;16;
---
Step 292:
Busy queue    : empty
Non-Busy queue: 16;11;
---
Step 293:
Busy queue    : empty
Non-Busy queue: 11;17;
---
Step 294:
Busy queue    : empty
Non-Busy queue: 17;28;
---
Step 295:
Busy queue    : empty
Non-Busy queue: 28;23;
---
Step 296:
Busy queue    : empty
Non-Busy queue: 23;53;
---
Step 297:
Busy queue    : empty
Non-Busy queue: 53;54;
---
Step 298:
Busy queue    : 32;
Non-Busy queue: 54;
---
Step 299:
Busy queue    : empty
Non-Busy queue: 54;61;
---
Step 300:
Busyness changed
Busy queue    : empty
Non-Busy queue: 61;36;
---
Step 301:
Busy queue    : empty
Non-Busy queue: 36;60;
---
Step 302:
Busy queue    : empty
Non-Busy queue: 60;37;
---
Step 303:
Busy queue    : empty
Non-Busy queue: 37;39;
---
Step 304:
Busy queue    : 43;
Non-Busy queue: 39;
---
Step 305:
Busy queue    : empty
Non-Busy queue: 39;42;
---
Step 306:
Busy queue    : empty
Non-Busy queue: 42;49;
---
Step 307:
Busy queue    : 52;
Non-Busy queue: 49;
---
Step 308:
Busy queue    : empty
Non-Busy queue: 49;2;
---
Step 309:
Busy queue    : empty
Non-Busy queue: 2;9;
---
Step 310:
Busyness changed
Busy queue    : empty
Non-Busy queue: 2;50;
---
Step 311:
Busy queue    : empty
Non-Busy queue: 50;29;
---
Step 312:
Busy queue    : 15;
Non-Busy queue: 29;
---
Step 313:
Busy queue    : 24;
Non-Busy queue: 29;
---
Step 314:
Busy queue    : 27;
Non-Busy queue: 29;
---
Step 315:
Busy queue    : empty
Non-Busy queue: 29;56;
---
Step 316:
Busy queue    : empty
Non-Busy queue: 56;46;
---
Step 317:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 318:
Busy queue    : empty
Non-Busy queue: 58;6;
---
Step 319:
Busy queue    : empty
Non-Busy queue: 6;8;
---
Step 320:
Busyness changed
Busy queue    : empty
Non-Busy queue: 6;14;
---
Step 321:
Busy queue    : 12;
Non-Busy queue: 14;
---
Step 322:
Busy queue    : empty
Non-Busy queue: 14;5;
---
Step 323:
Busy queue    : empty
Non-Busy queue: 5;41;
---
Step 324:
Busy queue    : empty
Non-Busy queue: 41;30;
---
Step 325:
Busy queue    : empty
Non-Busy queue: 30;19;
---
Step 326:
Busy queue    : 35;
Non-Busy queue: 19;
---
Step 327:
Busy queue    : empty
Non-Busy queue: 19;55;
---
Step 328:
Busy queue    : empty
Non-Busy queue: 55;13;
---
Step 329:
Busy queue    : empty
Non-Busy queue: 13;1;
---
Step 330:
Busyness changed
Busy queue    : empty
Non-Busy queue: 1;18;
---
Step 331:
Busy queue    : 22;
Non-Busy queue: 18;
---
Step 332:
Busy queue    : empty
Non-Busy queue: 18;38;
---
Step 333:
Busy queue    : empty
Non-Busy queue: 38;45;
---
Step 334:
Busy queue    : empty
Non-Busy queue: 45;31;
---
Step 335:
Busy queue    : 59;
Non-Busy queue: 31;
---
Step 336:
Busy queue    : empty
Non-Busy queue: 31;44;
---
Step 337:
Busy queue    : empty
Non-Busy queue: 44;25;
---
Step 338:
Busy queue    : empty
Non-Busy queue: 25;3;
---
Step 339:
Busy queue    : empty
Non-Busy queue: 3;21;
---
Step 340:
Busyness changed
Busy queue    : empty
Non-Busy queue: 21;20;
---
Step 341:
Busy queue    : empty
Non-Busy queue: 20;33;
---
Step 342:
Busy queue    : empty
Non-Busy queue: 33;34;
---
Step 343:
Busy queue    : 40;
Non-Busy queue: 34;
---
Step 344:
Busy queue    : empty
Non-Busy queue: 34;48;
---
Step 345:
Busy queue    : empty
Non-Busy queue: 48;57;
---
Step 346:
Busy queue    : empty
Non-Busy queue: 57;4;
---
Step 347:
Busy queue    : empty
Non-Busy queue: 4;47;
---
Step 348:
Busy queue    : empty
Non-Busy queue: 47;7;
---
Step 349:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 350:
Busyness changed
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 351:
Busy queue    : empty
Non-Busy queue: 26;10;
---
Step 352:
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 353:
Busy queue    : empty
Non-Busy queue: 62;16;
---
Step 354:
Busy queue    : empty
Non-Busy queue: 16;11;
---
Step 355:
Busy queue    : empty
Non-Busy queue: 11;17;
---
Step 356:
Busy queue    : empty
Non-Busy queue: 17;28;
---
Step 357:
Busy queue    : empty
Non-Busy queue: 28;23;
---
Step 358:
Busy queue    : empty
Non-Busy queue: 23;53;
---
Step 359:
Busy queue    : 32;
Non-Busy queue: 53;
---
Step 360:
Busyness changed
Busy queue    : empty
Non-Busy queue: 32;54;
---
Step 361:
Busy queue    : empty
Non-Busy queue: 54;61;
---
Step 362:
Busy queue    : 36;
Non-Busy queue: 61;
---
Step 363:
Busy queue    : 60;
Non-Busy queue: 61;
---
Step 364:
Busy queue    : empty
Non-Busy queue: 61;37;
---
Step 365:
Busy queue    : empty
Non-Busy queue: 37;43;
---
Step 366:
Busy queue    : empty
Non-Busy queue: 43;39;
---
Step 367:
Busy queue    : empty
Non-Busy queue: 39;42;
---
Step 368:
Busy queue    : empty
Non-Busy queue: 42;52;
---
Step 369:
Busy queue    : empty
Non-Busy queue: 52;49;
---
Step 370:
Busyness changed
Busy queue    : empty
Non-Busy queue: 49;9;
---
Step 371:
Busy queue    : empty
Non-Busy queue: 9;2;
---
Step 372:
Busy queue    : 50;
Non-Busy queue: 2;
---
Step 373:
Busy queue    : empty
Non-Busy queue: 2;15;
---
Step 374:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 375:
Busy queue    : empty
Non-Busy queue: 24;27;
---
Step 376:
Busy queue    : empty
Non-Busy queue: 27;29;
---
Step 377:
Busy queue    : empty
Non-Busy queue: 29;56;
---
Step 378:
Busy queue    : empty
Non-Busy queue: 56;46;
---
Step 379:
Busy queue    : empty
Non-Busy queue: 46;58;
---
Step 380:
Busyness changed
Busy queue    : 8;
Non-Busy queue: 58;
---
Step 381:
Busy queue    : empty
Non-Busy queue: 58;6;
---
Step 382:
Busy queue    : 12;
Non-Busy queue: 6;
---
Step 383:
Busy queue    : 14;
Non-Busy queue: 6;
---
Step 384:
Busy queue    : empty
Non-Busy queue: 6;5;
---
Step 385:
Busy queue    : empty
Non-Busy queue: 5;41;
---
Step 386:
Busy queue    : empty
Non-Busy queue: 41;30;
---
Step 387:
Busy queue    : empty
Non-Busy queue: 30;35;
---
Step 388:
Busy queue    : empty
Non-Busy queue: 35;19;
---
Step 389:
Busy queue    : empty
Non-Busy queue: 19;55;
---
Step 390:
Busyness changed
Busy queue    : empty
Non-Busy queue: 55;13;
---
Step 391:
Busy queue    : empty
Non-Busy queue: 13;1;
---
Step 392:
Busy queue    : empty
Non-Busy queue: 1;22;
---
Step 393:
Busy queue    : empty
Non-Busy queue: 22;18;
---
Step 394:
Busy queue    : empty
Non-Busy queue: 18;38;
---
Step 395:
Busy queue    : empty
Non-Busy queue: 38;45;
---
Step 396:
Busy queue    : empty
Non-Busy queue: 45;59;
---
Step 397:
Busy queue    : empty
Non-Busy queue: 59;31;
---
Step 398:
Busy queue    : empty
Non-Busy queue: 31;44;
---
Step 399:
Busy queue    : empty
Non-Busy queue: 44;25;
---
Step 400:
Busyness changed
Busy queue    : empty
Non-Busy queue: 25;3;
---
Step 401:
Busy queue    : empty
Non-Busy queue: 3;21;
---
Step 402:
Busy queue    : 20;
Non-Busy queue: 21;
---
Step 403:
Busy queue    : 33;
Non-Busy queue: 21;
---
Step 404:
Busy queue    : empty
Non-Busy queue: 21;40;
---
Step 405:
Busy queue    : empty
Non-Busy queue: 40;34;
---
Step 406:
Busy queue    : empty
Non-Busy queue: 34;48;
---
Step 407:
Busy queue    : empty
Non-Busy queue: 48;57;
---
Step 408:
Busy queue    : empty
Non-Busy queue: 57;4;
---
Step 409:
Busy queue    : empty
Non-Busy queue: 4;47;
---
Step 410:
Busyness changed
Busy queue    : empty
Non-Busy queue: 47;7;
---
Step 411:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 412:
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 413:
Busy queue    : empty
Non-Busy queue: 26;10;
---
Step 414:
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 415:
Busy queue    : empty
Non-Busy queue: 62;16;
---
Step 416:
Busy queue    : 11;
Non-Busy queue: 16;
---
Step 417:
Busy queue    : empty
Non-Busy queue: 16;17;
---
Step 418:
Busy queue    : empty
Non-Busy queue: 17;28;
---
Step 419:
Busy queue    : 23;
Non-Busy queue: 28;
---
Step 420:
Busyness changed
Busy queue    : empty
Non-Busy queue: 23;53;
---
Step 421:
Busy queue    : empty
Non-Busy queue: 53;32;
---
Step 422:
Busy queue    : empty
Non-Busy queue: 32;54;
---
Step 423:
Busy queue    : empty
Non-Busy queue: 54;36;
---
Step 424:
Busy queue    : 60;
Non-Busy queue: 36;
---
Step 425:
Busy queue    : empty
Non-Busy queue: 36;61;
---
Step 426:
Busy queue    : empty
Non-Busy queue: 61;37;
---
Step 427:
Busy queue    : 43;
Non-Busy queue: 37;
---
Step 428:
Busy queue    : empty
Non-Busy queue: 37;39;
---
Step 429:
Busy queue    : 42;
Non-Busy queue: 39;
---
Step 430:
Busyness changed
Busy queue    : empty
Non-Busy queue: 42;52;
---
Step 431:
Busy queue    : empty
Non-Busy queue: 52;49;
---
Step 432:
Busy queue    : empty
Non-Busy queue: 49;9;
---
Step 433:
Busy queue    : empty
Non-Busy queue: 9;50;
---
Step 434:
Busy queue    : empty
Non-Busy queue: 50;2;
---
Step 435:
Busy queue    : empty
Non-Busy queue: 2;15;
---
Step 436:
Busy queue    : empty
Non-Busy queue: 15;24;
---
Step 437:
Busy queue    : empty
Non-Busy queue: 24;27;
---
Step 438:
Busy queue    : empty
Non-Busy queue: 27;29;
---
Step 439:
Busy queue    : empty
Non-Busy queue: 29;56;
---
Step 440:
Busyness changed
Busy queue    : empty
Non-Busy queue: 56;46;
---
Step 441:
Busy queue    : empty
Non-Busy queue: 46;8;
---
Step 442:
Busy queue    : empty
Non-Busy queue: 8;58;
---
Step 443:
Busy queue    : empty
Non-Busy queue: 58;12;
---
Step 444:
Busy queue    : empty
Non-Busy queue: 12;14;
---
Step 445:
Busy queue    : empty
Non-Busy queue: 14;6;
---
Step 446:
Busy queue    : 5;
Non-Busy queue: 6;
---
Step 447:
Busy queue    : empty
Non-Busy queue: 6;41;
---
Step 448:
Busy queue    : empty
Non-Busy queue: 41;30;
---
Step 449:
Busy queue    : 35;
Non-Busy queue: 30;
---
Step 450:
Busyness changed
Busy queue    : empty
Non-Busy queue: 35;19;
---
Step 451:
Busy queue    : empty
Non-Busy queue: 19;55;
---
Step 452:
Busy queue    : empty
Non-Busy queue: 55;13;
---
Step 453:
Busy queue    : 1;
Non-Busy queue: 13;
---
Step 454:
Busy queue    : empty
Non-Busy queue: 13;22;
---
Step 455:
Busy queue    : 18;
Non-Busy queue: 22;
---
Step 456:
Busy queue    : empty
Non-Busy queue: 22;38;
---
Step 457:
Busy queue    : empty
Non-Busy queue: 38;45;
---
Step 458:
Busy queue    : empty
Non-Busy queue: 45;59;
---
Step 459:
Busy queue    : empty
Non-Busy queue: 59;31;
---
Step 460:
Busyness changed
Busy queue    : empty
Non-Busy queue: 31;44;
---
Step 461:
Busy queue    : 25;
Non-Busy queue: 44;
---
Step 462:
Busy queue    : empty
Non-Busy queue: 44;3;
---
Step 463:
Busy queue    : 20;
Non-Busy queue: 3;
---
Step 464:
Busy queue    : empty
Non-Busy queue: 3;33;
---
Step 465:
Busy queue    : empty
Non-Busy queue: 33;21;
---
Step 466:
Busy queue    : empty
Non-Busy queue: 21;40;
---
Step 467:
Busy queue    : empty
Non-Busy queue: 40;34;
---
Step 468:
Busy queue    : empty
Non-Busy queue: 34;48;
---
Step 469:
Busy queue    : empty
Non-Busy queue: 48;57;
---
Step 470:
Busyness changed
Busy queue    : empty
Non-Busy queue: 57;4;
---
Step 471:
Busy queue    : empty
Non-Busy queue: 4;47;
---
Step 472:
Busy queue    : empty
Non-Busy queue: 47;7;
---
Step 473:
Busy queue    : empty
Non-Busy queue: 7;51;
---
Step 474:
Busy queue    : empty
Non-Busy queue: 51;26;
---
Step 475:
Busy queue    : empty
Non-Busy queue: 26;10;
---
Step 476:
Busy queue    : empty
Non-Busy queue: 10;62;
---
Step 477:
Busy queue    : empty
Non-Busy queue: 62;11;
---
Step 478:
Busy queue    : empty
Non-Busy queue: 11;16;
---
Step 479:
Busy queue    : 17;
Non-Busy queue: 16;
---
Step 480:
Busyness changed
Busy queue    : empty
Non-Busy queue: 17;28;
---
```
